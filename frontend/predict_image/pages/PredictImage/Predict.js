import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { InfoBox, PageContent } from 'components'
import { RunPredict } from 'comps/predict_image/components'

class Predict extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { isLoaded } = this.props
    return (
      <PageContent>
        <Helmet>
          <title>Predict Image</title>
        </Helmet>
        <h1>Predict!</h1>
        <RunPredict />
      </PageContent>)
  }
}

export default compose(
)(Predict)
