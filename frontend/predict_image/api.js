import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function predict(uri) {
  return v1(`/predict_image${uri}`)
}

export default class Predict {

}
