import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { List } from 'immutable';
import './result-data.scss'
import { CheckboxList } from 'components'
import { v1 } from 'api'

class ResultData extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      reloadStr: ''
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    const { isLoaded } = this.props
    if (!isLoaded) {
      this.timeout = setTimeout(() => {
        this.setState({ isLoaded: true })
      }, 5000);
    }
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    const { id, error } = this.props
    const { isLoaded, reloadStr } = this.state
    if (!isLoaded || error) {
      return (<button onClick={()=>{this.setState({ isLoaded: !isLoaded })}}>Reload</button>)
    }
    return (
      <div className='result-data'>
        <img src={v1(`/predict_image/download/${id}?reloadStr=${reloadStr}`)} />
        <a className='download' href={v1(`/predict_image/download/${id}`)} ><button>Download</button></a>
        <button className='reload' onClick={()=>{this.setState({ reloadStr: ""+(new Date()).getSeconds()})}}>Reload</button>
      </div>
    )
  }
}
export default compose(
)(ResultData)
