# predict_image

Component for prediction of image data.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/predict_image.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.predict',
    'bundles.predict_image',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.predict.routes'),
    include('bundles.predict_image.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Predictimage,
} from 'comps/predict_image/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  PredictImage: 'PredictImage',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.PredictImage,
    path: '/predict_image',
    component: PredictImage,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.PredictImage} />
    ...
</div>
```
