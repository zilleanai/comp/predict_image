flask-unchained>=0.7.6
pandas
scikit-learn
numba
tqdm
git+https://github.com/zilleanai/zworkflow
Pillow
opencv-python
torch
torchvision

