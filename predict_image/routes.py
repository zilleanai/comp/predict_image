from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import PredictImageController


routes = lambda: [
    prefix('/api/v1', [
        prefix('/predict_image', [
            controller(PredictImageController),
        ]),
    ]),
]
