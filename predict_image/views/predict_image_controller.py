import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

import pandas as pd
from werkzeug.utils import secure_filename


class PredictImageController(Controller):
    pass

    @route('/download/<string:id>')
    def download(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        data_io = BytesIO(cached)
        data_io.seek(0)
        filename = id+'.png'
        return send_file(data_io, attachment_filename=filename, as_attachment=True)
